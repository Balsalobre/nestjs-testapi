import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProductModule } from './product/product.module';
import { MongooseModule } from '@nestjs/mongoose';
import { keys } from './config/keys';

@Module({
  imports: [ProductModule,
    MongooseModule.forRoot(
      keys.dbConnectionStrin,
      {
        useNewUrlParser: true
      }
    )],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
