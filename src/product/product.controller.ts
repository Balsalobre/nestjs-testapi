import { Controller, Get, Post, Put, Delete, Res, HttpStatus, Body, Param, NotFoundException, Query } from '@nestjs/common';
import { CreateProductDTO } from './dto/product.dto';
import { ProductService } from './product.service';
import { async } from 'rxjs/internal/scheduler/async';
import { ApiUseTags, ApiResponse, ApiBearerAuth } from '@nestjs/swagger';

@ApiUseTags('PRODUCTOS')
@Controller('product')
export class ProductController {

    constructor(private productService: ProductService){}

 @Post('/create')
 @ApiResponse({ status: 201, description: 'The recording has been completed successfully.'})
 @ApiResponse({ status: 403, description: 'Forbidden.'})
 @ApiResponse({ status: 400, description: 'Bad Request.'})

 async createPost(@Res() res, @Body() createProductDTO: CreateProductDTO) {
    const product = await this.productService.createProduct(createProductDTO);
    // console.log(createProductDTO);
    return res.status(HttpStatus.OK).json({
        message: 'Product Successfully Created',
        // product: product
        // nuevas versiones de JS
        product
    });
 }

 @Get('/')
 @ApiResponse({ status: 200, description: 'Products retrieved successfully.'})
 async getProducts(@Res() res) {
    const products = await this.productService.getProducts();
    res.status(HttpStatus.OK).json({
        products
    })
 }

 @Get('/:productID')
 @ApiResponse({ status: 200, description: 'Product retrieved successfully.'})
 @ApiResponse({ status: 404, description: 'Product does not exist.'})
 async getProduct(@Res() res, @Param('productID') productID) {
    const product = await this.productService.getProduct(productID);
    if (!product) throw new NotFoundException('Product Does not exists');
    return res.status(HttpStatus.OK).json(product);
 }

 @Delete('/delete')
 @ApiResponse({ status: 200, description: 'Product deleted successfully.'})
 @ApiResponse({ status: 400, description: 'Bad Request.'})
 async deleteProduct(@Res() res, @Query('productID') productID) {
    const productDeleted = await this.productService.deleteProduct(productID);
    if (!productDeleted) throw new NotFoundException('Product Does not exists');
    return res.status(HttpStatus.OK).json({
        message: 'Product Deleted Succesfully',
        productDeleted
    });
 }

 @Put('/update')
 @ApiResponse({ status: 200, description: 'Product updated successfully.'})
 @ApiResponse({ status: 400, description: 'Bad Request.'})
 async updateProduct(@Res() res,
                     @Body() createProductDTO: CreateProductDTO,
                     @Query('productID') productID)  {
                     const updatedProduct = await this.productService.updateProduct(productID, createProductDTO);
                     if (!updatedProduct) throw new NotFoundException('Product Does not exists');
                     return res.status(HttpStatus.OK).json({
                        message: 'Product Updated Succcesfully',
                        updatedProduct
                     });
 }
}
