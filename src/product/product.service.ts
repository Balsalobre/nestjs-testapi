import { Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { Product } from './interfaces/product.interface';
import { CreateProductDTO } from './dto/product.dto';

@Injectable()
export class ProductService {
    constructor(@InjectModel('Product') private readonly productModel: Model<Product>){}

    async getProducts(): Promise<Product[]> {
        const products = await this.productModel.find();
        return  products;
    }

    async getProduct(productID: string): Promise<Product> {
        const product = await this.productModel.findById(productID);
        return product;
    }

    async createProduct(createProductDTO: CreateProductDTO): Promise<Product> {
        const product =  new this.productModel(createProductDTO);
        await product.save();
        return product;
    }
    // La promesa tb puede ser de tipo <any>
    async deleteProduct(produrctID: string): Promise<Product> {
        const product = await this.productModel.findByIdAndDelete(produrctID);
        return product;
    }
    async updateProduct(produrctID: string, createProductDTO: CreateProductDTO): Promise<Product> {
        const updateProduct = await this.productModel.findByIdAndUpdate(
                                        produrctID, createProductDTO,{new: true});
        return updateProduct;
    }
}
