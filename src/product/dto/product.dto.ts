import { ApiModelProperty } from '@nestjs/swagger';
export class CreateProductDTO {
    @ApiModelProperty({description: 'Nombre del producto'})
    readonly name: string;
    @ApiModelProperty({description: 'Descripción del producto'})
    readonly description: string;
    @ApiModelProperty({description: 'Imagen del producto'})
    readonly imageUrl: string;
    @ApiModelProperty({description: 'Precio del producto'})
    readonly price: number;
    @ApiModelProperty({description: 'Fecha de creación'})
    readonly createdAt: Date;
}
